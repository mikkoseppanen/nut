# Nut

Utilities for calculating monthly wages from coding nut data.

## Acquiring

`go get -u bitbucket.org/mikkoseppanen/nut`

## Testing

`go test bitbucket.org/mikkoseppanen/nut`

## Usage example

```go

inputData := `
  Person Name,Person ID,Date,Start,End
  Test Tester,1,2.3.2014,6:00,14:00
`

// Pass a reader for the string data to parser. Could be
// any reader.
reader := strings.NewReader(inputData)

// Init new CSV parser
parser := nut.NewCSVParser(reader)

// Generate report from parser data.
report, err := nut.GenerateReport(parser)
if err != nil {
  log.Fatalln(err.Error())
}
```
