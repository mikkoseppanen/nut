package nut

import "time"

const (
	// BaseWage is the wage per hour for all work, in cents
	BaseWage = 375
	// EveningCompensation is added to base wage for hours between 18:00 and 06:00,
	// in cents.
	EveningCompensation = 115
	// OvertimeFactorA is a multiplier for BaseWage for first two overtime hours
	OvertimeFactorA = 1.25
	// OvertimeFactorB is a multiplier for BaseWage for first two overtime hours
	// after OvertimeFactorA
	OvertimeFactorB = 1.50
	// OvertimeFactorC is a multiplier for BaseWage for overtime hours after
	// OvertimeFactorB
	OvertimeFactorC = 2.0
	// RegularWorkMax is the number of hours after which overtime is activated.
	RegularWorkMax = (time.Hour * 8.0)
)

// RegularHours is a pair of timestamps that mark a time window during which
// work is considered to happen during regular hours.
// Inclusive at start, exclusive at end.
type RegularHours struct {
	Start         time.Time
	StartTomorrow time.Time
	End           time.Time
}

// NewRegularHoursFromDate returns regular hours for a given timestamps date.
func NewRegularHoursFromDate(dt time.Time) RegularHours {
	y, m, d := dt.Date()

	s := time.Date(y, m, d, 6, 0, 0, 0, dt.Location())
	e := time.Date(y, m, d, 18, 0, 0, 0, dt.Location())

	return RegularHours{Start: s, End: e, StartTomorrow: s.AddDate(0, 0, 1)}
}

// Compensation defines fields for compensation calculation results
type Compensation struct {
	HoursTotal    time.Duration
	HoursRegular  time.Duration
	HoursEvening  time.Duration
	HoursOvertime time.Duration
	Wage          int
}

// Sum sums the struct values in argument to the receiver
func (c *Compensation) Sum(x Compensation) {
	c.HoursTotal += x.HoursTotal
	c.HoursEvening += x.HoursEvening
	c.HoursRegular += x.HoursRegular
	c.HoursOvertime += x.HoursOvertime
	c.Wage += x.Wage
}

// Equal tells if two compensation structs are equal.
func (c Compensation) Equal(x Compensation) bool {
	return c.HoursTotal == x.HoursTotal &&
		c.HoursEvening == x.HoursEvening &&
		c.HoursRegular == x.HoursRegular &&
		c.HoursOvertime == x.HoursOvertime &&
		c.Wage == x.Wage
}

// DailyCompensation keeps a broken down report of daily hours and total wage in
// context of a single working day.
type DailyCompensation struct {
	Compensation
}

// AddShift incrementally adds the given shifts hours into the DailyCompensation
func (dc *DailyCompensation) AddShift(shift *Shift) {
	hours := shift.Duration()

	// If regular work cutoff is reached, count everything towards overtime.
	if dc.HoursTotal >= RegularWorkMax {
		dc.HoursOvertime += hours

	} else {
		// Get a breakdown of the slice time usage, we give the number of regular
		// hours left until overtime kicks in.
		brk := shift.BreakdownClipped(RegularWorkMax - dc.HoursTotal)

		dc.HoursEvening += brk.Evening
		dc.HoursRegular += brk.Regular
		dc.HoursOvertime += brk.Overtime
	}

	// Update total
	dc.HoursTotal += hours

	// Calculate money
	dc.Wage = Round(dc.HoursRegular.Hours() * BaseWage)

	if dc.HoursEvening > 0 {
		dc.Wage += Round(dc.HoursEvening.Hours() * (BaseWage + EveningCompensation))
	}

	if dc.HoursOvertime > 0 {
		oth := dc.HoursOvertime.Hours()

		if oth > 4 {
			dc.Wage += Round((oth - 4) * BaseWage * OvertimeFactorC)
			oth = 4
		}

		if oth > 2 {
			dc.Wage += Round((oth - 2) * BaseWage * OvertimeFactorB)
			oth = 2
		}

		dc.Wage += Round(oth * BaseWage * OvertimeFactorA)
	}
}
