package nut

import (
	"testing"
	"time"
)

var base = Compensation{HoursTotal: time.Hour * 10, Wage: 1555}
var double = Compensation{HoursTotal: time.Hour * 20, Wage: 3050}

// TestSum tests Sum method of Compensation struct
func TestSum(t *testing.T) {
	dest := base

	// test adding empty, shouldn't change
	dest.Sum(Compensation{})
	if dest.Equal(base) == false {
		t.Error("Adding empty changed result")
	}

	// Try doubling
	dest.Sum(dest)
	if dest.Equal(base) == true {
		t.Error("Still equal after adding self")
	}
}

// TestEqual tests Equal method of Compensation struct
func TestEqual(t *testing.T) {
	// test that clearly distinct values aren't a match
	if base.Equal(double) == true {
		t.Error("Base shouldn't be equal to double")
	}

	// Equal to self should always succeed
	if base.Equal(base) == false {
		t.Error("Base not equal to self")
	}

	// Shouldn't be equal if one changes
	test := base
	test.Wage++
	if test.Equal(base) == true {
		t.Error("Still equal after wage was changed")
	}

	test = base
	test.HoursTotal++
	if test.Equal(base) == true {
		t.Error("Still equal after total hours was changed")
	}
}
