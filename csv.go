package nut

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"strings"
	"time"
)

// CSVHeader is the expected format for CSV document header.
var CSVHeader = []string{"Person Name", "Person ID", "Date", "Start", "End"}

const (
	// CSVDateFmt is the expected format for date in CSV data
	CSVDateFmt = "2.1.2006"
	// CSVTimeFmt is the expected format for time in CSV data
	CSVTimeFmt = "15:04"
	// CSVDateTimeFmt combines date & time formats to a complete datetime format.
	CSVDateTimeFmt = CSVDateFmt + " " + CSVTimeFmt
)

// CSVParser implements an Parses for CSV documents
type CSVParser struct {
	rd io.Reader
}

// NewCSVParser returns a new CSV parser ready for use
func NewCSVParser(r io.Reader) *CSVParser {
	return &CSVParser{rd: r}
}

// GetAll attempts to read & parse all entries from the parses input reader.
func (p *CSVParser) GetAll() ([]Shift, error) {
	r := csv.NewReader(p.rd)

	records, err := r.ReadAll()
	if err != nil {
		return nil, err
	}

	if len(records) == 0 {
		return nil, errors.New("Found no rows in input data")
	}

	if VerifyCSVHeader(records[0]) == false {
		return nil, errors.New("Unexpected header in input data")
	}

	res := []Shift{}
	for _, record := range records[1:] {
		shift, err := ParseCSVShift(record)
		if err != nil {
			return nil, err
		}

		res = append(res, *shift)
	}

	return res, nil
}

// ParseCSVDateTime returns a time.Time struct parsed from given date and time.
// Expected formatting is CSVDateTimeFmt.
// Values are parsed using local timezone.
func ParseCSVDateTime(date, hours string) (time.Time, error) {
	t, err := time.ParseInLocation(CSVDateTimeFmt, fmt.Sprintf("%s %s", date, hours), time.Local)
	if err != nil {
		return t, fmt.Errorf("Error parsing CSV date '%s,%s': %s", date, hours, err.Error())
	}

	return t, nil
}

// VerifyCSVHeader returns if the given CSV row matches the expected header
// definition.
func VerifyCSVHeader(row []string) bool {
	if len(row) != len(CSVHeader) {
		return false
	}

	for c := range CSVHeader {
		if strings.EqualFold(row[c], CSVHeader[c]) == false {
			return false
		}
	}

	return true
}

// ParseCSVShift parses CSV shift (row) into a Shift struct
func ParseCSVShift(row []string) (*Shift, error) {
	if len(row) != 5 {
		return nil, fmt.Errorf("Invalid column length for row '%v'", row)
	}

	if row[0] == "" || row[1] == "" {
		return nil, fmt.Errorf("Invalid data in row '%v'", row)
	}

	s, err := ParseCSVDateTime(row[2], row[3])
	if err != nil {
		return nil, err
	}

	e, err := ParseCSVDateTime(row[2], row[4])
	if err != nil {
		return nil, err
	}

	if e.Before(s) || e.Equal(s) {
		e = e.AddDate(0, 0, 1)
	}

	return NewShift(row[1], row[0], s, e), nil
}
