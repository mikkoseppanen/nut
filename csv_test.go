package nut

import (
	"testing"
	"time"
)

// Datetime formatter, sans zone info, we'll use ParseInLocation to get the
// local timezone.
var dateFmt = "2006-01-02T15:04"

// TestHeaderVerifyValidInput tests that header verifier to succeed with correct input
func TestHeaderVerifyValidInput(t *testing.T) {
	// Verify against itself. Should always work
	if VerifyCSVHeader(CSVHeader) == false {
		t.Error("Expected header should verify, but didn't")
	}

	// Verify against mixed case versions.
	if VerifyCSVHeader([]string{"Person name", "person id", "DATE", "sTarT", "End"}) == false {
		t.Error("Mixed case header with valid field names should verify, but didn't")
	}
}

// TestHeaderVerifyInvalidInput tests that header verifier to fail with invalid input
func TestHeaderVerifyInvalidInput(t *testing.T) {
	if VerifyCSVHeader(nil) == true {
		t.Error("Nil header validated as true")
	}

	if VerifyCSVHeader([]string{}) == true {
		t.Error("Empty header validated as true")
	}

	// Correct length, invalid content
	if VerifyCSVHeader([]string{"name", "id", "date", "start", "end"}) == true {
		t.Error("Invalid header validated as true")
	}
}

// TestCSVDateParsing tests that given dates give correct time.Time structs
func TestCSVDateParsing(t *testing.T) {
	cmp, _ := time.ParseInLocation(dateFmt, "2016-06-23T12:00", time.Local)
	csvDate, err := ParseCSVDateTime("23.6.2016", "12:00")
	if err != nil {
		t.Error("Parsing date failed, when it should succeed")
	}

	if csvDate.Equal(cmp) == false {
		t.Error("Start date didn't parse correctly")
	}

	// Test fractional hour timestamp
	cmp, _ = time.ParseInLocation(dateFmt, "2016-06-23T12:45", time.Local)
	csvDate, err = ParseCSVDateTime("23.6.2016", "12:45")
	if err != nil {
		t.Error("Parsing date failed, when it should succeed")
	}

	if csvDate.Equal(cmp) == false {
		t.Error("Start date didn't parse correctly")
	}
}

// TestCSVParseShiftValidInput tests that parser returns a valid Shift from valid input
func TestCSVParseShiftValidInput(t *testing.T) {
	s, _ := ParseCSVDateTime("1.2.2016", "08:00")
	e, _ := ParseCSVDateTime("1.2.2016", "16:00")

	cmp := Shift{
		ID:    "1",
		Name:  "Jacob",
		Start: s,
		End:   e,
	}

	shift, err := ParseCSVShift([]string{"Jacob", "1", "1.2.2016", "08:00", "16:00"})
	if err != nil {
		t.Errorf("Parsing CSV into Shift failed when it should've succeeded: %s", err)
	}

	if shift.Equal(cmp) == false {
		t.Errorf("ParseCSVShift returned unexpected results\nExpected %v\nGot %v", cmp, shift)
	}
}

// TestCSVParseShiftValidInputCrossMidnight tests that parser returns a valid
// Shift from valid input where midnight boundary is crossed
func TestCSVParseShiftValidInputCrossMidnight(t *testing.T) {
	s, _ := ParseCSVDateTime("1.2.2016", "08:00")
	e, _ := ParseCSVDateTime("2.2.2016", "04:00")

	cmp := Shift{
		ID:    "1",
		Name:  "Jacob",
		Start: s,
		End:   e,
	}

	shift, err := ParseCSVShift([]string{"Jacob", "1", "1.2.2016", "08:00", "04:00"})
	if err != nil {
		t.Errorf("Parsing CSV into Shift failed when it should've succeeded: %s", err)
	}

	if shift.Equal(cmp) == false {
		t.Errorf("ParseCSVShift returned unexpected results\nExpected %v\nGot %v", cmp, shift)
	}
}

// TestCSVParseShiftValidInputIdenticalHours tests that parser returns a valid
// Shift from valid input where hours are identical (assuming 24h shift)
func TestCSVParseShiftValidInputIdenticalHours(t *testing.T) {
	s, _ := ParseCSVDateTime("1.2.2016", "08:00")
	e, _ := ParseCSVDateTime("2.2.2016", "08:00")

	cmp := Shift{
		ID:    "1",
		Name:  "Jacob",
		Start: s,
		End:   e,
	}

	shift, err := ParseCSVShift([]string{"Jacob", "1", "1.2.2016", "08:00", "08:00"})
	if err != nil {
		t.Errorf("Parsing CSV into Shift failed when it should've succeeded: %s", err)
	}

	if shift.Equal(cmp) == false {
		t.Errorf("ParseCSVShift returned unexpected results\nExpected %v\nGot %v", cmp, shift)
	}
}

// TestCSVParseShiftInvalidInput tests that parser returns an error with clearly
// invalid data.
func TestCSVParseShiftInvalidInput(t *testing.T) {
	_, err := ParseCSVShift([]string{"Jacob", "1", "1.2.201", "08:00", "16:00"})
	if err == nil {
		t.Error("Parsing CSV into Shift succeeded with invalid date")
	}

	_, err = ParseCSVShift([]string{"Jacob", "1", "1.2.2016", "0800", "16:00"})
	if err == nil {
		t.Error("Parsing CSV into Shift succeeded with invalid start hour")
	}

	_, err = ParseCSVShift([]string{"Jacob", "1", "1.2.2016", "08:00", "1600"})
	if err == nil {
		t.Error("Parsing CSV into Shift succeeded with invalid end hour")
	}

	_, err = ParseCSVShift([]string{"", "Jacob", "1.2.2016", "08:00", "16:00"})
	if err == nil {
		t.Error("Parsing CSV into Shift succeeded with empty ID")
	}

	_, err = ParseCSVShift([]string{"1", "", "1.2.2016", "08:00", "16:00"})
	if err == nil {
		t.Error("Parsing CSV into Shift succeeded with empty name")
	}

	_, err = ParseCSVShift([]string{"Jacob", "1", "08:00", "16:00"})
	if err == nil {
		t.Error("Parsing CSV into Shift succeeded with too short row")
	}

	_, err = ParseCSVShift([]string{"Jacob", "1", "1.2.2016", "08:00", "16:00", "...."})
	if err == nil {
		t.Error("Parsing CSV into Shift succeeded with too long row")
	}
}
