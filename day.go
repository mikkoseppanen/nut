package nut

import (
	"fmt"
	"time"
)

// Day contains shifts for a single date and allows for calculation of totals.
type Day struct {
	Date   time.Time
	Shifts []Shift
}

// NewDay returns a new Day struct initialized to the given date.
func NewDay(dt time.Time) *Day {
	// Use same timezone as original time struct
	y, m, d := dt.Date()

	return &Day{Date: time.Date(y, m, d, 0, 0, 0, 0, dt.Location())}
}

// AddShift adds a new shift to the day and sorts the shifts array by start time.
func (d *Day) AddShift(s Shift) {
	d.Shifts = append(d.Shifts, s)
	ShiftsBy(ShiftsByStart).Sort(d.Shifts)
}

// Compensation calculates wage and hour breakdown for the day.
func (d *Day) Compensation() *DailyCompensation {
	res := &DailyCompensation{}

	// Iterate shifts and calculate incrementally.
	for i := range d.Shifts {
		res.AddShift(&d.Shifts[i])
	}

	return res
}

// String returns a string serialization of the days data.
func (d Day) String() string {
	return fmt.Sprintf(
		"{Day %s, %d shifts}",
		d.Date.Format("02.01.2006"),
		len(d.Shifts),
	)
}
