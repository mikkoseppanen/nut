package nut

import (
	"testing"
	"time"
)

type dayCompensationCase struct {
	exp    Compensation
	shifts [][]string
}

var casesSingle = []dayCompensationCase{
	// Only regular
	dayCompensationCase{
		exp: Compensation{
			HoursRegular: time.Hour * 3,
			HoursTotal:   time.Hour * 3,
			Wage:         Round(BaseWage * 3),
		},
		shifts: [][]string{
			[]string{"Jacob", "1", "1.2.2016", "15:00", "18:00"},
		},
	},

	// Regular + evening
	dayCompensationCase{
		exp: Compensation{
			HoursRegular: time.Hour * 3,
			HoursEvening: time.Hour * 3,
			HoursTotal:   time.Hour * 6,
			Wage:         Round(BaseWage*3 + ((BaseWage + EveningCompensation) * 3)),
		},
		shifts: [][]string{
			[]string{"Jacob", "1", "1.2.2016", "15:00", "21:00"},
		},
	},

	// Regular + evening + overtimeA
	dayCompensationCase{
		exp: Compensation{
			HoursRegular:  time.Hour * 4,
			HoursEvening:  time.Hour * 4,
			HoursOvertime: time.Hour * 2,
			HoursTotal:    time.Hour * 10,
			Wage: Round(BaseWage*4) +
				Round(((BaseWage + EveningCompensation) * 4)) +
				Round((BaseWage * OvertimeFactorA * 2)),
		},
		shifts: [][]string{
			[]string{"Jacob", "1", "1.2.2016", "14:00", "00:00"},
		},
	},

	// Regular + evening + overtimeA + overtimeB
	dayCompensationCase{
		exp: Compensation{
			HoursRegular:  time.Hour * 4,
			HoursEvening:  time.Hour * 4,
			HoursOvertime: time.Hour * 4,
			HoursTotal:    time.Hour * 12,
			Wage: Round(BaseWage*4) +
				Round(((BaseWage + EveningCompensation) * 4)) +
				Round((BaseWage * OvertimeFactorA * 2)) +
				Round((BaseWage * OvertimeFactorB * 2)),
		},
		shifts: [][]string{
			[]string{"Jacob", "1", "1.2.2016", "14:00", "02:00"},
		},
	},

	// Regular + evening + overtimeA + overtimeB + overtimeC
	dayCompensationCase{
		exp: Compensation{
			HoursRegular:  time.Hour * 4,
			HoursEvening:  time.Hour * 4,
			HoursOvertime: time.Hour * 5,
			HoursTotal:    time.Hour * 13,
			Wage: Round(BaseWage*4) +
				Round(((BaseWage + EveningCompensation) * 4)) +
				Round((BaseWage * OvertimeFactorA * 2)) +
				Round((BaseWage * OvertimeFactorB * 2)) +
				Round((BaseWage * OvertimeFactorC * 1)),
		},
		shifts: [][]string{
			[]string{"Jacob", "1", "1.2.2016", "14:00", "03:00"},
		},
	},
}

var casesMany = []dayCompensationCase{
	dayCompensationCase{
		exp: Compensation{
			HoursRegular: time.Hour * 3,
			HoursEvening: time.Hour * 2,
			HoursTotal:   time.Hour * 5,
			Wage: Round(BaseWage*3) +
				Round(((BaseWage + EveningCompensation) * 2)),
		},
		shifts: [][]string{
			[]string{"Jacob", "1", "1.2.2016", "15:00", "18:00"},
			[]string{"Jacob", "1", "1.2.2016", "19:00", "21:00"},
		},
	},
	dayCompensationCase{
		exp: Compensation{
			HoursRegular:  time.Hour * 3,
			HoursEvening:  time.Hour * 5,
			HoursOvertime: time.Hour * 5,
			HoursTotal:    time.Hour * 13,
			Wage: Round(BaseWage*3) +
				Round(((BaseWage + EveningCompensation) * 5)) +
				Round((BaseWage * OvertimeFactorA * 2)) +
				Round((BaseWage * OvertimeFactorB * 2)) +
				Round((BaseWage * OvertimeFactorC * 1)),
		},
		shifts: [][]string{
			[]string{"Jacob", "1", "1.2.2016", "01:00", "06:00"},
			[]string{"Jacob", "1", "1.2.2016", "12:00", "15:00"},
			[]string{"Jacob", "1", "1.2.2016", "16:15", "18:15"},
			[]string{"Jacob", "1", "1.2.2016", "19:00", "22:00"},
		},
	},
}

// TestDaySingleNoShifts test compensation calculation with no shifts
func TestDaySingleNoShifts(t *testing.T) {
	d := NewDay(time.Now())
	c := d.Compensation()

	if c.Equal(Compensation{}) == false {
		t.Error("Compensation non-empty with a day without shifts")
	}
}

// TestDaySingleShift test compensation calculation with single shift
func TestDaySingleShift(t *testing.T) {
	for i, test := range casesSingle {
		shift, _ := ParseCSVShift(test.shifts[0])
		d := NewDay(shift.Start)
		d.AddShift(*shift)

		c := d.Compensation()
		if c.Equal(test.exp) == false {
			t.Errorf("Unexpected results from test %d.\nExpected %v\nGot %v", i, test.exp, c)
		}
	}
}

// TestDayManyShifts test compensation calculation with multiple shifts
func TestDayManyShifts(t *testing.T) {
	for i, test := range casesMany {
		shift, _ := ParseCSVShift(test.shifts[0])
		d := NewDay(shift.Start)
		d.AddShift(*shift)

		for _, row := range test.shifts[1:] {
			shift, _ := ParseCSVShift(row)
			d.AddShift(*shift)
		}

		c := d.Compensation()
		if c.Equal(test.exp) == false {
			t.Errorf("Unexpected results from test %d.\nExpected %v\nGot %v", i, test.exp, c)
		}
	}
}
