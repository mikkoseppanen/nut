package nut

import (
	"fmt"
	"sort"
)

// Employee contains calculated wages & hours for one employee
type Employee struct {
	ID   string
	Name string
	Days map[string]*Day
}

// NewEmployee returns an employee struct initialized with given arguments
func NewEmployee(id, name string) Employee {
	return Employee{ID: id, Name: name, Days: map[string]*Day{}}
}

// AddShift adds a new shift to the employees shifts
func (e *Employee) AddShift(s Shift) {
	var day *Day
	var ok bool

	// Lookup correct day for the shift
	if day, ok = e.Days[s.DateString()]; !ok {
		e.Days[s.DateString()] = NewDay(s.Start)
		day = e.Days[s.DateString()]
	}

	day.AddShift(s)
}

// Compensation returns a compensation report based on the recorded work
func (e *Employee) Compensation() *Compensation {
	res := &Compensation{}

	for d := range e.Days {
		dc := e.Days[d].Compensation()
		res.Sum(dc.Compensation)
	}

	return res
}

// String returns a string representation of the Employee struct
func (e Employee) String() string {
	return fmt.Sprintf("{Employee %s, %s}", e.ID, e.Name)
}

// EmployeesBy is the type of a "less" function that defines ordering of its Employee arguments.
type EmployeesBy func(a, b *Employee) bool

// Sort sorts the given list of Employees according to the function
func (by EmployeesBy) Sort(in []Employee) {
	s := &employeeSorter{
		items: in,
		by:    by,
	}

	sort.Sort(s)
}

// employeeSorter joins a sort function and a list of items to sort
type employeeSorter struct {
	items []Employee
	by    func(a, b *Employee) bool
}

// Len is part of sort.Interface.
func (s *employeeSorter) Len() int {
	return len(s.items)
}

// Swap is part of sort.Interface.
func (s *employeeSorter) Swap(i, j int) {
	s.items[i], s.items[j] = s.items[j], s.items[i]
}

// Less is part of sort.Interface. It is implemented by calling the "by" closure in the sorter.
func (s *employeeSorter) Less(i, j int) bool {
	return s.by(&s.items[i], &s.items[j])
}

// EmployeesByName is a sorter function for EmployeesBy that will sort by name
func EmployeesByName(a, b *Employee) bool {
	return a.Name < b.Name
}

// EmployeesByID is a sorter function for EmployeesBy that will sort by ID
func EmployeesByID(a, b *Employee) bool {
	return a.ID < b.ID
}
