package nut

// Parser describes an interface for reading shift data from various decoders.
type Parser interface {
	GetAll() ([]Shift, error)
}
