package nut

import (
	"fmt"
	"time"
)

// Report contains calculated sums of hours and wages for each employee in
// parsed dataset.
type Report struct {
	// Timestamp of first recorded shift
	From time.Time
	// Timestamp of last recorded shifts end
	To time.Time
	// Employees in the report
	Employees []Employee
}

// GenerateReport generates a report using the given parses as input
func GenerateReport(p Parser) (*Report, error) {
	shifts, err := p.GetAll()
	if err != nil {
		return nil, err
	}

	// sort shifts based on timestamp.
	ShiftsBy(ShiftsByStart).Sort(shifts)

	// Result is generated into this report struct
	res := &Report{}

	// Set first & last timestamp. We use start timestamp for both, since even if
	// shift ends on another date, the shift time is still recorded on the starting
	// date.
	if len(shifts) > 0 {
		res.From = shifts[0].Start
		res.To = shifts[len(shifts)-1].Start
	}

	// Keep a map of employees by ID to make it easier to look up existing entries
	temp := map[string]int{}

	// Then iterate the shifts, generate Employee entries as new ones emerge.
	for i := range shifts {
		var idx int
		var ok bool
		var emp *Employee

		// If shift ID isn't in temp map, we create a new employee
		if idx, ok = temp[shifts[i].ID]; !ok {
			idx = len(res.Employees)
			temp[shifts[i].ID] = idx
			res.Employees = append(res.Employees, NewEmployee(shifts[i].ID, shifts[i].Name))

		}

		emp = &res.Employees[idx]

		// Add shift to the employee
		emp.AddShift(shifts[i])
	}

	// Todo: calculate all totals.

	return res, nil
}

// Sort sorts the employee results by given sorter
func (r *Report) Sort(by EmployeesBy) {
	EmployeesBy(by).Sort(r.Employees)
}

// DateString returns the date string of the report (report months)
func (r Report) DateString() string {
	s := fmt.Sprintf("%s", r.From.Format("01/2006"))

	// Last result from different month, means we have more than one months worth.
	if r.From.Year() != r.To.Year() || r.From.Month() != r.To.Month() {
		// Alter header to indicate the longer time period
		s += fmt.Sprintf(" - %s", r.To.Format("01/2006"))
	}

	return s
}

// String returns a string serialization of the report data
func (r Report) String() string {
	// start with report header
	return fmt.Sprintf("{Report %s}", r.DateString())
}
