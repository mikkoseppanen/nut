package nut

import "math"

// Round method, since golang doesn't implement one in stdlib.
func Round(x float64) int {
	return int(x + math.Copysign(0.5, x))
}

// ToFixed to get a fixed precision truncation of given float.
func ToFixed(x float64, precision int) float64 {
	powd := math.Pow(10, float64(precision))
	return float64(Round(x*powd)) / powd
}
