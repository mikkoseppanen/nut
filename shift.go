package nut

import (
	"fmt"
	"sort"
	"time"
)

// Shift describes a single shift of work
type Shift struct {
	Name  string
	ID    string
	Start time.Time
	End   time.Time
}

// ShiftBreakdown contains a breakdown of hours into regular, evening pay and
// overtime.
type ShiftBreakdown struct {
	Regular  time.Duration
	Evening  time.Duration
	Overtime time.Duration
	Total    time.Duration
}

// Equal compares two ShiftBreakdown structs and returns if they're equal
func (sb ShiftBreakdown) Equal(x ShiftBreakdown) bool {
	return sb.Regular == x.Regular &&
		sb.Evening == x.Evening &&
		sb.Overtime == x.Overtime &&
		sb.Total == x.Total
}

// Equal compares two shifts and returns if they're equal
func (s Shift) Equal(x Shift) bool {
	return s.Name == x.Name &&
		s.ID == x.ID &&
		s.Start.Equal(x.Start) &&
		s.End.Equal(x.End)
}

// String returns a string representation of the shift
func (s Shift) String() string {
	return fmt.Sprintf(
		"{Shift %s, %s, %s - %s}",
		s.ID,
		s.Name,
		s.Start.Format(time.RFC3339),
		s.End.Format(time.RFC3339),
	)
}

// Hours returns the duration of the shift in hours
func (s *Shift) Hours() float64 {
	return s.Duration().Hours()
}

// Duration returns the duration of the shift
func (s *Shift) Duration() time.Duration {
	return s.End.Sub(s.Start)
}

// DateString returns the shifts date as a normalized string
func (s *Shift) DateString() string {
	return s.Start.Format("2006-01-02")
}

// Breakdown returns a breakdown of shift work time, split into regular, evening
// and overtime hours, assuming overtime after number of hours of work set by
// RegularWorkMax have passed.
func (s Shift) Breakdown() *ShiftBreakdown {
	return s.BreakdownClipped(RegularWorkMax)
}

// BreakdownClipped returns a breakdown of shift work time, split into
// regular, evening and overtime hours.
// When hour total is equal or greater than regMax, all remaining hours are
// added to overtime.
// If regMax is greater than constant RegularWorkMax, RegularWorkMax is used
// instead.
func (s Shift) BreakdownClipped(regMax time.Duration) *ShiftBreakdown {
	// Shortcut if hours left is 0.
	if regMax == 0 {
		return &ShiftBreakdown{
			Overtime: s.Duration(),
			Total:    s.Duration(),
		}
	}

	// Check that regular working hours don't exceed the predefined maximum.
	if regMax > RegularWorkMax {
		regMax = RegularWorkMax
	}

	// Result struct
	res := &ShiftBreakdown{Total: s.End.Sub(s.Start)}

	// Get regular hours for the starting date. We'll use this to decide if hours
	// go towards regular or evening compensation.
	rh := NewRegularHoursFromDate(s.Start)

	// Calculate a time when overtime kicks in.
	otAt := s.Start.Add(regMax)

	// If overtime starts before shift end, clip shift end to overtime begin
	// and add the rest straight to overtime.
	if otAt.Before(s.End) {
		res.Overtime = s.End.Sub(otAt)
		s.End = otAt
	}

	// Calculate based on where the work starts. Since we've already clipped
	// overtime off, we can safely assume that shifts never cross regular/evening
	// boundary twice. We also know that we don't need to worry about adding to
	// overtime.
	switch {
	// Work started before regular hours
	case s.Start.Before(rh.Start):
		if s.End.Before(rh.Start) {
			// Stopped before regular work begun. Only 'evening' pay.
			res.Evening = s.End.Sub(s.Start)

		} else {
			// Evening pay until regular hours, rest regular hours.
			res.Evening = rh.Start.Sub(s.Start)
			res.Regular = s.End.Sub(rh.Start)
		}

	case s.Start.Equal(rh.End) || s.Start.After(rh.End):
		if s.End.Before(rh.StartTomorrow) {
			// Stopped before regular work begun. Only 'evening' pay.
			res.Evening = s.End.Sub(s.Start)

		} else {
			// Evening pay until regular hours, rest regular hours.
			res.Evening = rh.StartTomorrow.Sub(s.Start)
			res.Regular = s.End.Sub(rh.StartTomorrow)
		}

	default: // Start during regular hours.
		if s.End.Before(rh.End) {
			// Only regular hours
			res.Regular = s.End.Sub(s.Start)

		} else {
			// First regular, rest evening
			res.Regular = rh.End.Sub(s.Start)
			res.Evening = s.End.Sub(rh.End)
		}
	}

	return res
}

// NewShift returns a new Shift with start and end set from arguments
func NewShift(id, name string, start, end time.Time) *Shift {
	s := &Shift{
		ID:    id,
		Name:  name,
		Start: start,
		End:   end,
	}

	return s
}

// NewShiftFromDuration returns a new Shift starting from start argument and
// ending calculated based on given duration from the start.
func NewShiftFromDuration(id, name string, start time.Time, duration time.Duration) *Shift {
	return NewShift(id, name, start, start.Add(duration))
}

// ShiftsBy is the type of a "less" function that defines ordering of its Shift arguments.
type ShiftsBy func(a, b *Shift) bool

// Sort sorts the given list of Shifts according to the function
func (by ShiftsBy) Sort(in []Shift) {
	s := &shiftSorter{
		items: in,
		by:    by,
	}

	sort.Sort(s)
}

// shiftSorter joins a sort function and a list of items to sort
type shiftSorter struct {
	items []Shift
	by    func(a, b *Shift) bool
}

// Len is part of sort.Interface.
func (s *shiftSorter) Len() int {
	return len(s.items)
}

// Swap is part of sort.Interface.
func (s *shiftSorter) Swap(i, j int) {
	s.items[i], s.items[j] = s.items[j], s.items[i]
}

// Less is part of sort.Interface. It is implemented by calling the "by" closure in the sorter.
func (s *shiftSorter) Less(i, j int) bool {
	return s.by(&s.items[i], &s.items[j])
}

// ShiftsByStart is a sorter function for ShiftsBy that will sort by starting time
func ShiftsByStart(a, b *Shift) bool {
	return a.Start.Before(b.Start)
}
