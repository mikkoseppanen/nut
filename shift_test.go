package nut

import (
	"testing"
	"time"
)

type breakDownCase struct {
	exp   ShiftBreakdown
	ot    time.Duration
	input []string
}

var casesBreakdownBasic = []breakDownCase{
	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "08:00", "10:30"},
		exp: ShiftBreakdown{
			Regular: (time.Hour * 2) + (time.Minute * 30),
			Total:   (time.Hour * 2) + (time.Minute * 30),
		},
	},

	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "05:45", "10:45"},
		exp: ShiftBreakdown{
			Regular: (time.Hour * 4) + (time.Minute * 45),
			Evening: (time.Minute * 15),
			Total:   (time.Hour * 5),
		},
	},

	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "17:45", "22:00"},
		exp: ShiftBreakdown{
			Evening: (time.Hour * 4),
			Regular: (time.Minute * 15),
			Total:   (time.Hour * 4) + (time.Minute * 15),
		},
	},

	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "22:00", "02:00"},
		exp: ShiftBreakdown{
			Evening: (time.Hour * 4),
			Total:   (time.Hour * 4),
		},
	},
	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "03:00", "03:00"},
		exp: ShiftBreakdown{
			Evening:  (time.Hour * 3),
			Regular:  (time.Hour * 5),
			Overtime: (time.Hour * 16),
			Total:    (time.Hour * 24),
		},
	},
	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "06:00", "06:00"},
		exp: ShiftBreakdown{
			Regular:  (time.Hour * 8),
			Overtime: (time.Hour * 16),
			Total:    (time.Hour * 24),
		},
	},
	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "18:00", "18:00"},
		exp: ShiftBreakdown{
			Evening:  (time.Hour * 8),
			Overtime: (time.Hour * 16),
			Total:    (time.Hour * 24),
		},
	},
	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "15:00", "15:00"},
		exp: ShiftBreakdown{
			Evening:  (time.Hour * 5),
			Regular:  (time.Hour * 3),
			Overtime: (time.Hour * 16),
			Total:    (time.Hour * 24),
		},
	},
	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "23:00", "23:00"},
		exp: ShiftBreakdown{
			Evening:  (time.Hour * 7),
			Regular:  (time.Hour * 1),
			Overtime: (time.Hour * 16),
			Total:    (time.Hour * 24),
		},
	},
}

func TestBreakdownBasic(t *testing.T) {
	for i, c := range casesBreakdownBasic {
		shift, _ := ParseCSVShift(c.input)
		bd := shift.Breakdown()

		if bd.Equal(c.exp) == false {
			t.Errorf("Error with basic breakdown test %d:\nExpected: %v, Got: %v", i, c.exp, bd)
		}
	}
}

var casesBreakdownFixedOt = []breakDownCase{
	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "08:00", "10:30"},
		ot:    (time.Hour * 3),
		exp: ShiftBreakdown{
			Regular: (time.Hour * 2) + (time.Minute * 30),
			Total:   (time.Hour * 2) + (time.Minute * 30),
		},
	},
	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "05:45", "10:45"},
		ot:    (time.Hour * 3),
		exp: ShiftBreakdown{
			Regular:  (time.Hour * 2) + (time.Minute * 45),
			Evening:  (time.Minute * 15),
			Overtime: (time.Hour * 2),
			Total:    (time.Hour * 5),
		},
	},
	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "17:45", "22:00"},
		ot:    (time.Hour * 3),
		exp: ShiftBreakdown{
			Evening:  (time.Hour * 2) + (time.Minute * 45),
			Regular:  (time.Minute * 15),
			Overtime: (time.Hour * 1) + (time.Minute * 15),
			Total:    (time.Hour * 4) + (time.Minute * 15),
		},
	},
	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "03:00", "03:00"},
		ot:    (time.Hour * 3),
		exp: ShiftBreakdown{
			Evening:  (time.Hour * 3),
			Overtime: (time.Hour * 21),
			Total:    (time.Hour * 24),
		},
	},
	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "15:00", "15:00"},
		ot:    (time.Hour * 3),
		exp: ShiftBreakdown{
			Regular:  (time.Hour * 3),
			Overtime: (time.Hour * 21),
			Total:    (time.Hour * 24),
		},
	},
	breakDownCase{
		input: []string{"Jacob", "1", "1.2.2016", "23:00", "23:00"},
		ot:    (time.Hour * 3),
		exp: ShiftBreakdown{
			Evening:  (time.Hour * 3),
			Overtime: (time.Hour * 21),
			Total:    (time.Hour * 24),
		},
	},
}

func TestBreakdownFixedOt(t *testing.T) {
	for i, c := range casesBreakdownFixedOt {
		shift, _ := ParseCSVShift(c.input)
		bd := shift.BreakdownClipped(c.ot)

		if bd.Equal(c.exp) == false {
			t.Errorf("Error with basic breakdown test %d:\nExpected: %v, Got: %v", i, c.exp, bd)
		}
	}
}
